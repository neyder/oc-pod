#Instalar

FROM registry.access.redhat.com/ubi8/ubi

RUN yum install -y ca-certificates curl gzip jq tar procps-ng wget rsync

RUN curl -L https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/4.9.28/openshift-client-linux.tar.gz | tar --directory /usr/bin -x -z -f - oc kubectl

VOLUME ["/opt/target"]

#ENTRYPOINT ["dnsmasq", "--no-daemon", "--log-queries", "--no-hosts", "--no-resolv", "--no-negcache", "--no-poll"]

CMD ["oc", "-h"]
